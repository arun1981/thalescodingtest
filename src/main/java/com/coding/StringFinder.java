package com.coding;

import java.io.IOException;
import java.util.List;

import static com.coding.ResultCode.FAILURE;
import static com.coding.ResultCode.SUCCESS;

public class StringFinder {

    public Result find(String textToSearch) throws IOException {

        List<List<String>> matrix = new GridReader().readGrid();

        int rowLength = matrix.size();
        int columnLength = matrix.get(0).size();

        int successCount = 0;

        for (int x = 0; x < rowLength; x++) {

            for (int y = 0; y < columnLength; y++) {

                if (isStringPresent(textToSearch, matrix, rowLength, columnLength, x, y)) {
                    successCount++;
                }

            }

        }

        return new Result(successCount > 0 ? SUCCESS : FAILURE, "\n\rThe string `" + textToSearch + "` was found " + successCount + " times");

    }

    private boolean isStringPresent(String textToSearch, List<List<String>> matrix, int mx, int my, int x, int y) {
        for (Direction direction : Direction.values()) {

            int length = textToSearch.length();
            if (isValidDirection(direction, x, y, length, mx, my)) {

                StringBuffer charBuffer = new StringBuffer();

                int currentX = x;
                int currentY = y;

                for (int c = 0; c < length; c++) {
                    String c1 = matrix.get(currentX).get(currentY);
                    charBuffer.append(c1);
                    currentX += direction.x;
                    currentY += direction.y;

                }

                String computedString = charBuffer.toString();

                if (computedString.trim().equalsIgnoreCase(textToSearch.trim())) {
                    System.out.println();
                    System.out.println("String " + textToSearch + " found starting at position" + "(" + x + "," + y + ") to (" + (currentX -= direction.x) + "," + (currentY -= direction.y) + ")");
                    return true;
                }

            }

        }
        return false;
    }

    public static boolean isValidDirection(Direction direction, int x, int y, int length, int mx, int my) {

        int xBoundary = x + direction.x * (length - 1);
        int yBoundary = y + direction.y * (length - 1);
        return !(xBoundary >= mx || xBoundary < 0 || yBoundary < 0 || yBoundary >= my);

    }
}