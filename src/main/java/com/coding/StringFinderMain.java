package com.coding;

import java.io.IOException;
import java.util.Scanner;

public class StringFinderMain {

    public static void main(String[] args) throws IOException {
        while (true) {
            System.out.print("\n\rEnter the string to be searched:");
            Result result = new StringFinder().find(new Scanner(System.in).nextLine());
            System.out.println(result.getMessage());
        }
    }

}
