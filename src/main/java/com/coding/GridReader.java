package com.coding;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class GridReader {

    public List<List<String>> readGrid() throws IOException {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("wordSearch.csv").getFile());

        List<List<String>> matrix = new ArrayList<>();

        try (Stream<String> stream = Files.lines(file.toPath(), Charset.defaultCharset())) {
            stream.forEach(e -> matrix.add(Arrays.asList(e.split(","))));
        }
        return matrix;
    }
}
