package com.coding;

public enum Direction {

    west(0,-1),
    northWest(-1, -1),
    north(-1, 0),
    northEast(-1, +1),
    east(0, +1),
    southEast(+1, +1),
    south(+1, 0),
    southWest(+1, -1);

    int x;
    int y;

    Direction(int x, int y){
    this.x = x;
    this.y = y;

    }

}
