package com.coding;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class StringFinderTest {

    @Test
    public void shouldfind() throws IOException {
        Result result = new StringFinder().find("logic");
        assertEquals(ResultCode.SUCCESS,result.getResultCode());
        assertEquals("\n\rThe string `logic` was found 1 times",result.getMessage());
    }

    @Test
    public void shouldNotfind() throws IOException {
        Result result = new StringFinder().find("logik");
        assertEquals(ResultCode.FAILURE,result.getResultCode());
        assertEquals("\n\rThe string `logik` was found 0 times",result.getMessage());
    }

    @Test
    public void shouldFindDiagonally() throws IOException {
        Result result = new StringFinder().find("aeu");
        assertEquals(ResultCode.SUCCESS,result.getResultCode());
        assertEquals("\n\rThe string `aeu` was found 1 times",result.getMessage());
    }

    @Test
    public void shouldFindHorizontally() throws IOException {
        Result result = new StringFinder().find("afh");
        assertEquals(ResultCode.SUCCESS,result.getResultCode());
        assertEquals("\n\rThe string `afh` was found 1 times",result.getMessage());
    }

    @Test
    public void shouldFindVertically() throws IOException {
        Result result = new StringFinder().find("adl");
        assertEquals(ResultCode.SUCCESS,result.getResultCode());
        assertEquals("\n\rThe string `adl` was found 1 times",result.getMessage());
    }

    @Test
    public void shouldFindDiagonalReverse() throws IOException {
        Result result = new StringFinder().find("esa");
        assertEquals(ResultCode.SUCCESS,result.getResultCode());
        assertEquals("\n\rThe string `esa` was found 1 times",result.getMessage());
    }

}